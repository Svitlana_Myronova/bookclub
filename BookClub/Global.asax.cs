﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Http;
using BookClub.App_Start;

namespace BookClub
{
    //Global.asax is used to handling higher level application events such as Application_Start, Application_End, Session_Start, Session_End etc.
    //Global.asax contains a Class representing your application as a whole. 
    //At run time, this file is parsed and compiled into a dynamically generated .NET Framework class derived from the HttpApplication base class. 
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}
