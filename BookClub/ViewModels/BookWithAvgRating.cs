﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BookClub.ViewModels
{
    public class BookWithAvgRating
    {
        public int BookId { get; set; }
        public string Title { get; set; }
        public string Genre { get; set; }
        public List<string> Authors { get; set; }

        [DisplayFormat(DataFormatString = "{0:#.##}")]
        public Double Rating { get; set; }
    }
}