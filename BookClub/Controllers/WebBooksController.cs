﻿using BookClub.ApiModels;
using BookClub.Models;
using BookClub.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace BookClub.Controllers
{
    [RoutePrefix("api/books")]
    public class WebBooksController : ApiController
    {

        readonly BookClubEntities DB = new BookClubEntities();

        [Route("")]
        [HttpGet]
        // GET: Books
        //[FromUri] attribute forces Web API to get the value of complex type from the query string
        public DataTableResponseModel GetBooks([FromUri] DataTableRequestModel dataTableRequestModel)
        {
            int draw = dataTableRequestModel.draw;
            int length = dataTableRequestModel.length;
            int start = dataTableRequestModel.start;
            DataTableSearchModel search = dataTableRequestModel.search;
            DataTableOrderModel[] order = dataTableRequestModel.order;
            DataTableColumnModel[] columns = dataTableRequestModel.columns;

            int recordsTotal = DB.Books.Count();
            int recordsFiltered = DB.Books.Count();
          
            try
            {

                //IQueryable<BookWithRating>
                var dataTotal =
                    from review in DB.Reviews
                    group review by review.BookId into bookGroup
                    select new BookWithAvgRating()
                    {
                        BookId = bookGroup.AsQueryable().FirstOrDefault().Book.BookId,
                        Title = bookGroup.AsQueryable().FirstOrDefault().Book.Title,
                        Genre = bookGroup.AsQueryable().FirstOrDefault().Book.Genre,
                        Authors = bookGroup.AsQueryable().FirstOrDefault().Book.Authors.Select(author => author.FirstName + " " +author.LastName).ToList(),
                        Rating = bookGroup.Average(ed => ed.Rating)
                    };

                IQueryable<BookWithAvgRating> dataTotalAfterSearch = dataTotal;

                if (!string.IsNullOrEmpty(search.value))
                {
                    string searchValue = search.value.ToLower();
                    dataTotalAfterSearch = dataTotal.Where(d => d.BookId.ToString().ToLower().Contains(searchValue)
                                                             || d.Title.ToLower().Contains(searchValue)
                                                             || d.Genre.ToLower().Contains(searchValue)
                                                             || d.Authors.Any(authorFullName => authorFullName.ToLower().Contains(searchValue))
                                                             || d.Rating.ToString().ToLower().Contains(searchValue));
                }

                IQueryable<BookWithAvgRating> dataTotalOrdered = dataTotalAfterSearch;

                for (int i=0; i<order.Length; i++)
                {
                    int columnNum = order[i].column;
                    string columnName = columns[columnNum].data;
                    string direction = order[i].dir;
                    bool ifDescending = string.Equals(direction, "desc");

                    switch (columnName)
                    {
                        case "BookId":
                            dataTotalOrdered = (ifDescending) ? dataTotalOrdered.OrderByDescending(d => d.BookId) : dataTotalOrdered.OrderBy(d => d.BookId);
                            break;
                        case "Title":
                            dataTotalOrdered = (ifDescending) ? dataTotalOrdered.OrderByDescending(d => d.Title) : dataTotalOrdered.OrderBy(d => d.Title);
                            break;
                        case "Genre":
                            dataTotalOrdered = (ifDescending) ? dataTotalOrdered.OrderByDescending(d => d.Genre) : dataTotalOrdered.OrderBy(d => d.Genre);
                            break;
                        case "Rating":
                            dataTotalOrdered = (ifDescending) ? dataTotalOrdered.OrderByDescending(d => d.Rating) : dataTotalOrdered.OrderBy(d => d.Rating);
                            break;                        
                        default:
                            dataTotalOrdered = dataTotalOrdered.OrderBy(d => d.BookId);
                            break;
                    }
                }

                var dataForPage = dataTotalOrdered.Skip(start).Take(length).ToList();

                var dataTableResponse = new DataTableResponseModel(draw, recordsTotal, recordsFiltered, dataForPage, "");

                return dataTableResponse;
            }
            catch (Exception e)
            {
                return new DataTableResponseModel(0, 0, 0, new List<BookWithAvgRating>(), "");
            }
        }
    }
}
