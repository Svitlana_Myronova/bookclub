﻿using BookClub.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookClub.ApiModels
{
    public class DataTableResponseModel
    {
        public DataTableResponseModel(int draw, int recordsTotal, int recordsFiltered, List<BookWithAvgRating> data, string error)
        {
            this.draw = draw;
            this.recordsTotal = recordsTotal;
            this.recordsFiltered = recordsFiltered;
            this.data = data;
            this.error = error;
        }

        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<BookWithAvgRating> data { get; set; }
        public string error { get; set; }
    }
}