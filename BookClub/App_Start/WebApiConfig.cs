﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;

namespace BookClub.App_Start
{
    public class WebApiConfig
    {
        public static void Register(HttpConfiguration configuration)
        {
            //Web API configuration and services

            // Enable attribute routing
            configuration.MapHttpAttributeRoutes();

            // Add default route using convention-based routing
            configuration.Routes.MapHttpRoute(
                name: "API Default", 
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional });

            //ASP.NET Web API returns JSON instead of XML
            configuration.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
        }
    }
}