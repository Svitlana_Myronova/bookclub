﻿using System.Web.Mvc;
using System.Web.Routing;

namespace BookClub
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}",
                defaults: new { controller = "Books", action = "Index" }
            );

            //custom routes are after the default route
            routes.MapRoute(
                name: "Book",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Book", action = "BookView", id = "" }
            );
        }
    }
}
