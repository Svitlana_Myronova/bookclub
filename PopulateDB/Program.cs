﻿using BookClub.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace PopulateDB
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Creating authors");
            List<Author> authors = CreateAuthors();

            //for checking:

            //authors.ForEach(delegate(Author author)
            //{
            //    Console.WriteLine(AuthorToString(author));
            //});

            Console.WriteLine("Creating books");
            List<Book> books = CreateBooks(authors);

            //for checking:

            //books.ForEach(delegate (Book book)
            //{
            //    Console.WriteLine(BookToString(book));
            //});  

            Console.WriteLine("Creating users");
            List<User> users = CreateUsers();

            //for checking:

            //users.ForEach(delegate (User user)
            //{
            //    Console.WriteLine(UserToString(user));
            //});

            Console.WriteLine("Creating reviews");
            List<Review> reviews = CreateReviews(books, users);

            //for checking:

            //reviews.ForEach(delegate (Review review)
            //{
            //    Console.WriteLine(ReviewToString(review));
            //});

            Console.WriteLine("Add reviews to books");
            AddReviewsToBook(reviews, books);

            Console.WriteLine("Add reviews to users");
            AddReviewsToUser(reviews, users);

            AddToDB(authors, books, users, reviews);

            Console.WriteLine("Done");
            Console.ReadLine();
        }

        public static string AuthorToString(Author a)
        {
            return a.AuthorId.ToString() + "[" + a.FirstName + " " + a.LastName + "]";
        }

        public static string BookToString(Book b)
        {
            return b.BookId.ToString() + " " + b.Title + " " + b.Description + " " + b.Genre;
        }

        public static string UserToString(User u)
        {
            return u.UserName + " " + u.Password + " " + u.LastName + " " + u.FirstName + " " + u.Email + " " + u.Country;
        }

        public static string ReviewToString(Review r)
        {
            return r.ReviewId.ToString() + " " + r.BookId + " " + r.UserName + " " + r.Rating + " " + r.Content;
        }

        public static String XelElementAtributValue(XElement Xel, string el, string atr)
        {
            if (Xel == null)
                return null;
            if (Xel.Element(el) == null)
                return null;
            if (Xel.Element(el).Attribute(atr) == null)
                return null;
            return Xel.Element(el).Attribute(atr).Value;
        }

        public static String XelAtributValue(XElement Xel, string atr)
        {
            if (Xel == null)
                return null;
            if (Xel.Attribute(atr) == null)
                return null;
            return Xel.Attribute(atr).Value;
        }

        private static List<Author> CreateAuthors()
        {
            XElement booksXml = XDocument.Load("../../xml/books.xml").Root;

            IEnumerable<XElement> booksXE = booksXml.Elements("book");

            List<Author> authors = new List<Author>();

            foreach (XElement bookEl in booksXE)
            {
                Author author = new Author
                {
                    //author.LastName = Convert.ToString(bookEl.Element("author").Attribute("lastName").Value);
                    LastName = Convert.ToString(XelElementAtributValue(bookEl, "author", "lastName")),
                    //author.FirstName = Convert.ToString(bookEl.Element("author").Attribute("firstName").Value);
                    FirstName = Convert.ToString(XelElementAtributValue(bookEl, "author", "firstName"))
                };

                authors.Add(author);
            }

            int authorId = 0;
            var list = (from author in authors
                        orderby author.FirstName, author.LastName ascending
                        select author).ToArray();

            authors.Clear();
            for (int i = 0; i < list.Length - 1; i++)
            {
                if (list[i].FirstName == list[i + 1].FirstName && list[i].LastName == list[i + 1].LastName)
                {
                    continue;
                }

                list[i].AuthorId = authorId;
                authors.Add(list[i]);
                authorId++;
            }
            list[list.Length - 1].AuthorId = authorId;
            authors.Add(list[list.Length - 1]);
            return authors;
        }

        private static List<Book> CreateBooks(List<Author> authors)
        {
            XElement booksXml = XDocument.Load("../../xml/books.xml").Root;

            IEnumerable<XElement> booksXE = booksXml.Elements("book");

            List<Book> books = new List<Book>();

            foreach (XElement bookEl in booksXE)
            {
                int id = Convert.ToInt32(bookEl.Attribute("id").Value);
                string title = Convert.ToString(bookEl.Element("title").Value);
                string description = Convert.ToString(bookEl.Element("description").Value);
                string genre = Convert.ToString(bookEl.Element("genre").Value);
                string lastName = Convert.ToString(XelElementAtributValue(bookEl, "author", "lastName"));
                string firstName = Convert.ToString(XelElementAtributValue(bookEl, "author", "firstName"));

                var bookParts = new
                {
                    Id = id,
                    Title = title,
                    Description = description,
                    Genre = genre,
                    AthourFirstName = firstName,
                    AthourLastName = lastName
                };

                Book book = new Book
                {
                    BookId = bookParts.Id,
                    Description = bookParts.Description,
                    Genre = bookParts.Genre,
                    Title = bookParts.Title,

                    Authors = new HashSet<Author>()
                };

                Author author = authors.Where(a => String.Equals(a.FirstName, bookParts.AthourFirstName) &&
                                                          String.Equals(a.LastName, bookParts.AthourLastName)).First();

                book.Authors.Add(author);

                books.Add(book);
            }

            return books;
        }

        private static List<User> CreateUsers()
        {
            XElement ratingsXml = XDocument.Load("../../xml/ratings.xml").Root;

            IEnumerable<XElement> ratingsXE = ratingsXml.Elements("user");

            List<User> users = new List<User>();

            foreach (XElement ratingEl in ratingsXE)
            {
                User user = new User
                {
                    UserName = Convert.ToString(XelAtributValue(ratingEl, "userId")),
                    Password = Convert.ToString(XelAtributValue(ratingEl, "userId")),

                    LastName = Convert.ToString(XelAtributValue(ratingEl, "lastName")),

                    FirstName = Convert.ToString(XelAtributValue(ratingEl, "userId")),
                    Email = Convert.ToString(XelAtributValue(ratingEl, "email")),

                    Country = Convert.ToString(XelAtributValue(ratingEl, "country"))
                };

                if (user.LastName == null)
                    user.LastName = "Reader";

                if (user.Country == null)
                    user.Country = "CAN";

                users.Add(user);
            }

            return users;
        }


        public static List<Review> CreateReviews(List<Book> books, List<User> users)
        {
            XElement ratingsXml = XDocument.Load("../../xml/ratings.xml").Root;

            IEnumerable<XElement> usersXE = ratingsXml.Elements("user");

            List<Review> reviews = new List<Review>();
            int reviewId = 0;
            foreach (XElement userEl in usersXE)
            {
                string userName = Convert.ToString(XelAtributValue(userEl, "userId"));
                string lastName = Convert.ToString(XelAtributValue(userEl, "lastName"));
                User user = users.Where(x => x.UserName == userName && x.LastName == lastName).First();

                IEnumerable<XElement> reviewsXE = userEl.Elements("review");
                foreach (XElement reviewEl in reviewsXE)
                {
                    int bookId = Convert.ToInt32(XelAtributValue(reviewEl, "bookId"));

                    Review review = new Review
                    {
                        ReviewId = reviewId,

                        Book = books.Where(x => x.BookId == bookId).First(),
                        BookId = bookId,

                        User = user,
                        UserName = user.UserName,

                        Rating = Convert.ToInt32(XelAtributValue(reviewEl, "rating")),
                        Content = Convert.ToString(XelAtributValue(reviewEl, "content"))
                    };

                    reviews.Add(review);

                    reviewId++;
                }
            }
            return reviews;
        }

        public static void AddReviewsToBook(List<Review> reviews, List<Book> books)
        {
            foreach (Book book in books)
            {
                foreach (Review review in reviews)
                {
                    book.Reviews = reviews.Where(x => x.BookId == book.BookId).Distinct().ToList();
                }
            }
        }

        public static void AddReviewsToUser(List<Review> reviews, List<User> users)
        {
            foreach (User user in users)
            {
                foreach (Review review in reviews)
                {
                    user.Reviews = reviews.Where(x => x.UserName == user.UserName).Distinct().ToList();
                }
            }
        }

        public static void AddToDB(List<Author> authors, List<Book> books, List<User> users, List<Review> reviews)
        {
            Console.WriteLine("Connecting to DB");
            BookClubEntities DB = new BookClubEntities();

            Console.WriteLine("Cleaning DB");
            DB.Reviews.RemoveRange(DB.Reviews);
            DB.Authors.RemoveRange(DB.Authors);
            DB.Books.RemoveRange(DB.Books);
            DB.Users.RemoveRange(DB.Users);

            try
            {
                Console.WriteLine("Saving changes to DB.");
                DB.SaveChanges();
            }
            catch (DbUpdateException e)
            {
                Console.WriteLine("Exception caught: {0}", e);
            }

            Console.WriteLine("Adding authors to DB");
            DB.Authors.AddRange(authors);

            Console.WriteLine("Adding books to DB");
            DB.Books.AddRange(books);

            Console.WriteLine("Adding users to DB");
            DB.Users.AddRange(users);

            Console.WriteLine("Adding reviews to DB");
            DB.Reviews.AddRange(reviews);

            try
            {
                Console.WriteLine("Saving changes to DB.");
                DB.SaveChanges();
            }
            catch (DbUpdateException e)
            {
                Console.WriteLine("Exception caught: {0}", e);
            }
        }
    }
}
