# Book Club

Book Club is a web application that is written in **C#** using **ASP.NET MVC**, web **APIs** and **Data Tables**. This website functions as a book club. When anyone connects to the site, before logging in, a summary page is presented, which consists of a list of the books. The user can sort books in different ways. A link associated with each book directs to a details page with all information and reviews. Only registered user can leave a review and add a new book. When a new user registers, he/she must provide information such as username, password, first name, last name, email, and country.

## Summary page

This view point has a pagination and shows only 10 books. The user can click *next* to see the next set of books or *previous* to show the previous ones. The user can sort books alphabetically, by genre and by average rating as well. Each title is a link that directs to a details page. Here, a registered user can also add a new book.

![Screenshort](Capture1.PNG)

## WIP: Details page

It shows the book title, genre, author(s) and a description, along with the average rating and all the reviews. A registered user can review the book. A review is comprised of a rating (scale of 1-5) and text.

## WIP: Navigation Bar

It consists of **Book Club**, **LogIn** and **About** items.
-   **Book Club** takes the user back to **Summary page**
-	**LogIn** directs the user to **LogIn page** that takes care of registration and login.
-	**About** describes the app.

## Database

**SQL database** was created on Microsoft Azure Cloud Service. Database tables were populated using 2 XML files and **LINQ-to-XML** and **LINQ-to-Entities**.




